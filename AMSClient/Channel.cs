﻿using Newtonsoft.Json;

namespace AMSClient
{
    /// <summary>
    /// En repræsenterer en push channel i Azure Mobile Services
    /// 
    /// </summary>
    public class Channel
    {
        public int Id { get; set; }
        
        /// <summary>
        /// Det her er den URI, som man skal bruge for at kontakte et device
        /// </summary>
        [JsonProperty(PropertyName = "uri")]
        public string Uri { get; set; }
    
        [JsonProperty(PropertyName = "owner")]
        public string Owner { get; set; }
    
        [JsonProperty(PropertyName = "deviceId")]
        public string DeviceId { get; set; }
    }
}