﻿using System;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.WindowsAzure.MobileServices;

namespace AMSClient
{
    public partial class MainPage : PhoneApplicationPage
    {
        #region Authentication
        private MobileServiceUser user;

        private async System.Threading.Tasks.Task Authenticate()
        {
            while (user == null)
            {
                string message;
                try
                {
                    user = await App.MobileService
                                    .LoginAsync(MobileServiceAuthenticationProvider.Twitter);
                    message = string.Format("You are now logged in - {0}", user.UserId);
                    App.ViewModel.Owner = user.UserId;
                }
                catch (InvalidOperationException)
                {
                    message = "You must log in. Login Required";
                }
                MessageBox.Show(message);
            }
        }
        #endregion 

        public MainPage()
        {
            InitializeComponent();
            DataContext = App.ViewModel;
            Loaded += MainPage_Loaded;
        }

        async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            await Authenticate();

            #region Push Notifications

            await App.Pusher.AcquirePushChannelFor(App.ViewModel.Owner);

            #endregion

            App.ViewModel.LoadData();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.ViewModel.Add();
        }
    }
}