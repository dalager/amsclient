﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AMSClient
{
    /// <summary>
    /// ViewModel, der er bundet til MainView
    /// </summary>
    public class MainViewModel:INotifyPropertyChanged
    {
        #region Private parts


        private ObservableCollection<Item> _items;
        private string _newItem;
        private string _owner;

        #endregion

        public ObservableCollection<Item> Items
        {
            get { return _items; }
            set { _items = value; NotifyPropertyChanged("Items"); }
        }

        public string NewItem
        {
            get { return _newItem; }
            set { _newItem = value; NotifyPropertyChanged("NewItem");}
        }

        public string Owner
        {
            get { return _owner; }
            set { _owner = value; NotifyPropertyChanged("Owner");}
        }

        public void Add()
        {
            if (string.IsNullOrWhiteSpace(NewItem))
            {
                return;
            }
            var item = new Item {Name = NewItem,Owner = App.ViewModel.Owner};
            Items.Add(item);
            SaveItem(item);
            NewItem = string.Empty;
            NotifyPropertyChanged("Items");
        }

        async public void SaveItem(Item item)
        {
            var itemTable = App.MobileService.GetTable<Item>();
            await itemTable.InsertAsync(item);
        }

        async public void LoadData()
        {
            var itemTable = App.MobileService.GetTable<Item>();
            var listAsync = await itemTable
                .Where(x=>x.Owner == Owner)
                .ToListAsync();
            Items = new ObservableCollection<Item>(listAsync);
            NotifyPropertyChanged("Items");
        }

        #region IPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }

        #endregion
    }
}