﻿using Newtonsoft.Json;

namespace AMSClient
{
    /// <summary>
    /// Et eksempel på noget, vi gemmer.
    /// </summary>
    public class Item
    {
        /// <summary>
        /// En int id skal altid være der
        /// </summary>
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "owner")]
        public string Owner { get; set; }
    }
}