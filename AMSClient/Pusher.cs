﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Phone.Info;
using Microsoft.Phone.Notification;
using Microsoft.WindowsAzure.MobileServices;

namespace AMSClient
{
    /// <summary>
    /// Hjælpeklasse til at håndtere Push Channels
    /// </summary>
    public class Pusher
    {
        public static HttpNotificationChannel CurrentChannel { get; private set; }

        public async Task AcquirePushChannelFor(string owner)
        {
            CurrentChannel = HttpNotificationChannel.Find("mychannel");
            if (CurrentChannel == null)
            {
                CurrentChannel = new HttpNotificationChannel("mychannel");
                CurrentChannel.Open();
            }
            
            CurrentChannel.HttpNotificationReceived += CurrentChannel_HttpNotificationReceived;
            CurrentChannel.ErrorOccurred += CurrentChannel_ErrorOccurred;
            CurrentChannel.ConnectionStatusChanged += CurrentChannel_ConnectionStatusChanged;
            CurrentChannel.ChannelUriUpdated += CurrentChannel_ChannelUriUpdated;
        }

        async void CurrentChannel_ChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            if (e.ChannelUri == null)
            {
                return;
            }
            await UpdateChannelOnServer();
        }

        private static async Task UpdateChannelOnServer()
        {
            IMobileServiceTable<Channel> channelTable = App.MobileService.GetTable<Channel>();
            Debug.WriteLine("Got channeltable");
            var deviceId = GetDeviceId();

            var listAsync =
                await
                channelTable.Where(x => x.Owner== App.ViewModel.Owner && x.DeviceId == deviceId)
                            .ToListAsync();
            Debug.WriteLine("got {0} existing channels. Prepare to delete.", listAsync.Count);
            if (listAsync.Any())
            {
                var channels = listAsync.ToArray();
                foreach (var chan in channels)
                {
                    await channelTable.DeleteAsync(chan);
                }
            }
            Debug.WriteLine("channels deleted");

            Debug.WriteLine("Inserting the new channel, with uri " + CurrentChannel.ChannelUri);
            var channel = new Channel
                {
                    Uri = CurrentChannel.ChannelUri.ToString(),
                    DeviceId = GetDeviceId(),
                    Owner = App.ViewModel.Owner
                };

            await channelTable.InsertAsync(channel);
        }


        void CurrentChannel_ConnectionStatusChanged(object sender, NotificationChannelConnectionEventArgs e)
        {
            Debug.WriteLine("connection status changed");
        }

        private static string GetDeviceId()
        {
            var deviceIdBytes = (byte[])DeviceExtendedProperties.GetValue("DeviceUniqueId");
            return Convert.ToBase64String(deviceIdBytes);
        }

        void CurrentChannel_ErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            Debug.WriteLine("Exception on push channel: " + e.Message);
            MessageBox.Show(e.Message, "Error on push channel", MessageBoxButton.OK);
        }

        async void CurrentChannel_HttpNotificationReceived(object sender, HttpNotificationEventArgs e)
        {
            // vi er ligeglade med payload. Vi er bare SÅ glade for et ping.
            Deployment.Current.Dispatcher.BeginInvoke(() => App.ViewModel.LoadData());
        }
    }
}